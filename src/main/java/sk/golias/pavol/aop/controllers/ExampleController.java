package sk.golias.pavol.aop.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.golias.pavol.aop.annotations.LogExecutionTime;

@RestController
public class ExampleController {

    @LogExecutionTime
    @GetMapping(path = "/")
    public String exampleEndpoint() throws InterruptedException {
        Thread.sleep(2000);
        return "TEST";
    }
}
